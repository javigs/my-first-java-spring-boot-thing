FROM tomcat:9
LABEL maintainer=””
ADD springapi.war /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["catalina.sh", "run"]

