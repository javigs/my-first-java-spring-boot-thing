package es.javigs.springapi;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
class TablaModelAssembler implements RepresentationModelAssembler<Tabla, EntityModel<Tabla>> {

  @Override
  public EntityModel<Tabla> toModel(Tabla entity) {

    return EntityModel.of(entity,
        linkTo(methodOn(TablaController.class).find(entity.getId())).withSelfRel(),
        linkTo(methodOn(TablaController.class).all()).withRel("tablas"));
  }
}