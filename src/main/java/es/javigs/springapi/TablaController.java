package es.javigs.springapi;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import es.javigs.springapi.dto.TablaDTO;

@RestController
class TablaController {

    private final TablaRepository repository;

    private final TablaModelAssembler assembler;

    TablaController(TablaRepository repository, TablaModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/tablas")
    public CollectionModel<EntityModel<Tabla>> all() {

        List<EntityModel<Tabla>> tablas = repository.findAll().stream()
            .map(this.assembler::toModel)
            .collect(Collectors.toList());

        return CollectionModel.of(tablas, linkTo(methodOn(TablaController.class).all()).withSelfRel());
    }

    @PostMapping("/tablas")
    public ResponseEntity<EntityModel<Tabla>> store(@RequestBody TablaDTO tablaDTO) {
    
        Tabla newTabla = new Tabla(tablaDTO.getColumn1());
        EntityModel<Tabla> entityModel = assembler.toModel(repository.save(newTabla));
          
        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @GetMapping("/tablas/{id}")
    public EntityModel<Tabla> find(@PathVariable Long id) {

        Tabla tabla = repository.findById(id).orElseThrow(() -> new TablaNotFoundException(id));

        return this.assembler.toModel(tabla);
    }

    @PutMapping("/tablas/{id}")
    public ResponseEntity<EntityModel<Tabla>> update(@RequestBody TablaDTO tablaDTO, @PathVariable Long id) {

        Tabla updatedTabla = repository.findById(id)
            .map(tabla -> {
                tabla.setColumn1(tablaDTO.getColumn1());
                return repository.save(tabla);
            })
            .orElseThrow(() -> new TablaNotFoundException(id));
      
        EntityModel<Tabla> entityModel = assembler.toModel(updatedTabla);
      
        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @DeleteMapping("/tablas/{id}")
    public ResponseEntity<Tabla> delete(@PathVariable Long id) {

        // Using imperative Optional
        Optional<Tabla> tabla = repository.findById(id);
        if (!tabla.isPresent()) {
            throw new TablaNotFoundException(id);
        }

        repository.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
