package es.javigs.springapi;

public class TablaNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    TablaNotFoundException(Long id) {
        
        super("Could not find " + id);
    }
}
