package es.javigs.springapi;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TablaRepository extends JpaRepository<Tabla, Long> {

}
