package es.javigs.springapi;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tabla_muchos_indices")
public class Tabla {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String column1;

    protected Tabla() {
    }

    public Tabla(String column1) {
        this.column1 = column1;
    }

    public Tabla(Long id, String column1) {
        this.id = id;
        this.column1 = column1;
    }

    @Override
    public String toString() {
        return String.format("Tabla[id=%d, column1='%s']", id, column1);
    }

    public Long getId() {
        return id;
    }

    public String getColumn1() {
        return column1;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }
}
