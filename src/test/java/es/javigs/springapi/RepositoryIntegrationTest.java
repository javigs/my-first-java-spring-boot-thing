package es.javigs.springapi;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class RepositoryIntegrationTest {
    
    @LocalServerPort
	private int port;
    
    @Autowired
    private TestRestTemplate rest;

    @Test
    void getTest() throws Exception {
        assertThat(this.rest.getForObject("http://localhost:" + port + "/tablas", String.class)).isNotNull();
    }
}
